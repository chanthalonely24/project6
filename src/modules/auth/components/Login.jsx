import { useState } from "react";
import { useAuth } from "./Auth";
import { login } from "../core/request";

const Login = () => {

    const [username, setUsername] = useState("kminchelle");
    const [pass, setPass] = useState("0lelplR");
    const [error, setError] = useState("");
    const { saveAuth } = useAuth();

    const onLogin = (event) => {
        event.preventDefault();
        login(username, pass).then((response) => {
            setError("");
            saveAuth(response.data.token)
        }).catch((error) => {
            setError(error.response.data.message);
        })
    }



    return <div className="container">
        <div className="row">`
            <div className="col-md-6 offset-md-3">
                <h2 className="text-center text-dark mt-5">Login Form</h2>
                <div className="text-center mb-5 text-dark">Made with bootstrap</div>
                <div className="card my-5">

                    <form className="card-body cardbody-color p-lg-5">

                        

                        <div className="mb-3">
                            <input type="text" className="form-control" id="Username" aria-describedby="emailHelp"
                                placeholder="User Name" onChange={(e) => setUsername(e.target.value)} />
                        </div>
                        <div className="mb-3">
                            <input type="password" className="form-control" id="password" placeholder="password"
                                onChange={(e) => setPass(e.target.value)} />
                        </div>
                        <div className="text-center">
                            <button onClick={onLogin} className="btn btn-color px-5 mb-5 w-100">Login</button>
                        </div>
                        <div className="text-center text-danger">{error}</div>

                    </form>
                </div>

            </div>
        </div>
    </div>;
}


export default Login;
