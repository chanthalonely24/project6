import { useEffect, useState } from "react";
import { getCategories, getListProduct, getListProductByCategory, searchProduct } from "../core/request";
import { useNavigate } from "react-router-dom";
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Badge from 'react-bootstrap/Badge';
import Stack from 'react-bootstrap/Stack';


const ListProducts = () => {

    const [products, setProducts] = useState([]);
    const [active, setActive] = useState("All");
    const [categories, setCategories] = useState([]);
    const [searchText, setSearchText] = useState("");
    const navigate = useNavigate();





    useEffect(() => {
        getListProduct(100).then((response) => {
            setProducts(response.data.products);
        });

        getCategories().then((response) => {
            setCategories(response.data);
        })
    }, []);


    useEffect(() => {
        active === "All" ?
            getListProduct(100).then((response) => {
                setProducts(response.data.products);
            })
            : getListProductByCategory(100, active).then((response) => {
                setProducts(response.data.products);
            });
    }, [active]);


    const onSearch = () => {
        searchProduct({ q: searchText }).then((response) => {
            setProducts(response.data.products);
        });
    }




    return <div className="container mt-5">
        <div className="w-70 d-flex justify mb-4">
            <Form className="d-flex justify-content-center px-5 w-50">
                <Form.Control
                    type="search"
                    placeholder="Search"
                    className="me-2"
                    aria-label="Search"
                    onChange={(e) => setSearchText(e.target.value)}
                />
                <Button onClick={onSearch} variant="outline-success">Search</Button>
            </Form>
        </div>

        <div className="px-4">
            <div className="d-flex overflow-x-scroll w-100 pb-3">
                <Badge onClick={() => setActive("All")} className="px-4 py-2 fs-6 mx-2" bg={active === "All" ? "primary" : "secondary"}>All</Badge>
                {categories.map((item, index) =>
                    <Badge onClick={() => setActive(item)} key={index} className="px-4 py-2 fs-6 mx-2 " bg={active === item ? "primary" : "secondary"}>{item}</Badge>)}
            </div>
        </div>


        <div className="d-flex flex-wrap justify-content-center">
            {products.length === 0 && <div>
                Not Found
            </div>}
            {products.map((product, index) => {
                return <div onClick={() => navigate(`product/${product.id}`)} className="mx-2 border rounded-2 my-2" style={{

                }} key={index}>
                    <img style={{
                        height: "200px",
                        width: "300px"
                    }} className="" src={product.thumbnail} alt="__" />
                    <p className="border-top px-2">{product.title}</p>
                </div>
            })}
        </div>
    </div>
}

export default ListProducts;